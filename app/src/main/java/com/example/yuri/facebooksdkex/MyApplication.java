package com.example.yuri.facebooksdkex;

import android.app.Application;

import com.facebook.FacebookSdk;

/**
 * Created by test on 2/26/16.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
