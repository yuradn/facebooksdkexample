package com.example.yuri.facebooksdkex;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private AccessToken accessToken;
    private PermissionFragment permissionFragment;

    private FacebookCallback facebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            // App code
            Log.e(TAG, "onSuccess " + loginResult.getAccessToken());
            accessToken = loginResult.getAccessToken();
            AccessToken.setCurrentAccessToken(accessToken);
            Profile.fetchProfileForCurrentAccessToken();
            Log.d(TAG, "Permisson: " + AccessToken.getCurrentAccessToken().getPermissions().toString());
            final GraphRequest request = new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    loginResult.getAccessToken().getUserId(),
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            /* handle the result */


                            Log.d(TAG, "Profile:\n" + response.toString());
                            JSONObject object = response.getJSONObject();
                            String socialId = getJS(object, "id");
                            String socialFirstName = getJS(object, "first_name");
                            String socialLastName = getJS(object, "last_name");
                            String socialName = getJS(object, "name");
                            String socialEmail = getJS(object, "email");
                            String socialLink = getJS(object, "link");
                            String socialCoverUrl = getCoverUrl(object);
                            String socialPictureUrl = getPictureUrl(object);
                        }

                        private String getCoverUrl(JSONObject object) {
                            String socialCoverUrl = "";
                            try {
                                JSONObject cover = object.getJSONObject("cover");
                                socialCoverUrl = getJS(cover, "source");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return socialCoverUrl;
                        }

                        private String getPictureUrl(JSONObject object) {
                            String socialPictureUrl = "";
                            try {
                                JSONObject picture = object.getJSONObject("picture");
                                JSONObject data = picture.getJSONObject("data");
                                socialPictureUrl = getJS(data, "url");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return socialPictureUrl;
                        }
                    }
            );
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,picture.width(200).height(200)" +
                    ",link,gender,birthday,email,cover,first_name, last_name");
            request.setParameters(parameters);
            request.executeAsync();

            toBirthdayRequest();
        }


        private String getJS(JSONObject o, String name) {
            String tmp = "";
            try {
                tmp = o.getString(name);
                Log.d(TAG, "Pole name: " + name + " value: " + tmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return tmp;
        }

        @Override
        public void onCancel() {
            // App code
            Log.e(TAG, "onCancel");
        }

        @Override
        public void onError(FacebookException exception) {
            // App code
            Log.e(TAG, "onError");
        }
    };

    private void toBirthdayRequest() {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject userMe, GraphResponse response) {
                        if(userMe!=null){
                            //...... do your things
                            Log.e(TAG, "JS2: "+userMe.toString()+"\n"+"Response2: "+response.toString());
                        }
                    }
                });

        Bundle parameters = new Bundle();

//Add the fields that you need, you dont forget add the right permission
        parameters.putString("fields", "email,id,name,picture,birthday,user_location, user_birthday, user_likes");
        request.setParameters(parameters);

//Now you can execute
        GraphRequest.executeBatchAsync(request);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);
        // FaceBook button
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "user_friends", "user_about_me"
                , "user_birthday", "user_education_history", "user_work_history", "user_status"));
        // Callback registration
        loginButton.registerCallback(callbackManager, facebookCallback);

        /*permissionFragment = new PermissionFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, permissionFragment)
                .commit();*/

        /*findViewById(R.id.btnStart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               *//* LoginManager.getInstance().logInWithReadPermissions(MainActivity.this, Arrays.
                        asList("user_friends", "user_status", "user_about_me", "user_work_history", "user_education_history"));*//*
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,timezone,work");
                new GraphRequest(
                        AccessToken.getCurrentAccessToken(),
                        "/" + AccessToken.getCurrentAccessToken().getUserId(),//+"/"+"devices",
                        parameters,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            public void onCompleted(GraphResponse response) {
                                Log.d(TAG, "Permisson: " + AccessToken.getCurrentAccessToken().getPermissions().toString());
                                Log.d(TAG, response.toString());
                            }
                        }
                ).executeAsync();
                *//* make the API call
                new GraphRequest(
                        AccessToken.getCurrentAccessToken(),
                        "/"+AccessToken.getCurrentAccessToken().getUserId()+"/permissions",
                        null,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            public void onCompleted(GraphResponse response) {
             handle the result
                                Log.d(TAG, response.toString());
                            }
                        }
                ).executeAsync();*//*
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
